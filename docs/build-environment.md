
## Python3 설치
from https://ttottoro.tistory.com/387

$ brew update

$ sudo rm /usr/local/bin/python*
$ sudo rm /usr/local/bin/pip*
$ sudo rm -Rf /Library/Frameworks/Python.framework/Versions/*

$ more .bash_profile
export PATH=/usr/local/bin/:/usr/local/sbin:${PATH}


$ brew install python3

$ brew upgrade python3

## 공용 Python Package 설치

$ cd <project_dir>
$ python3 -m venv Py3-11Env

$ source Py3-11Env/bin/activate
(Py3-11Env) $ 

## by-pythonBeginner

### part-1

./requirements.txt

```
fastapi==0.103.2
pydantic==2.4.2
uvicorn==0.23.2
```

./Dockefile

```
FROM faucet/python3:11.0.0
WORKDIR /usr/yjlee/app
COPY ./requirements.txt /usr/yjlee/app/requirements.txt
RUN pip3 install --no-cache-dir --upgrade -r /usr/yjlee/app/requirements.txt
```

./docker-compose.yml

```
version: "3.9"
services:
    fastapi:
        container_name: fastapi
        build: .
        working_dir: /usr/yjlee/app
        command: uvicorn main:app --host=0.0.0.0 --port=8000 --reload
        environment:
            DEBUG: 1
        volumes:
            - ./app:/usr/yjlee/app
        ports:
            - "80:8000"
        restart: on-failure
```

$ docker compose up

in browser

http://0.0.0.0:80/

{"Hello":"World!"}

### part-2

main.py 수정

```python
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class Track(BaseModel):
    title: str
    artist: str
    album: str | None = None
    year: int
    label: str | None = None

tracks: list[Track] = []


@app.get("/")
async def root():
    return {"Hello": "World!"}

@app.get("/tracks")
async def get_tracks():
    return tracks

@app.post("/tracks")
async def create_track(track: Track):
  tracks.append(track)
  return track
```

in browser

http://localhost/docs

### part-3
특별한 것 없었음. 그냥 따라하면 됩니다.

```python
# ./app/main.py

from fastapi import FastAPI
from routers import tracks
# from pydantic import BaseModel

app = FastAPI()

fallback = {404: {"description": "NotFound"}}
app.include_router(tracks.router, prefix="", tags=["Tracks"])
# app.include_router(tracks.router, prefix="/tracks", tags=["Tracks"], response=fallback)

# class Track(BaseModel):
#     title: str
#     artist: str
#     album: str | None = None
#     year: int
#     label: str | None = None

# tracks: list[Track] = []


@app.get("/")
async def root():
    return {"Hello": "World!"}

# @app.get("/tracks")
# async def get_tracks():
#     return tracks

# @app.post("/tracks")
# async def create_track(track: Track):
#   tracks.append(track)
#   return track
```

```python
# ./app/routers/tracks.py

from fastapi import APIRouter, HTTPException
from pydantic import BaseModel


router = APIRouter(
  prefix = "/tracks",
  tags=["Tracks"],
#   response = ({404: {"description" : "Not Found"}})
)

class Track(BaseModel):
    id: int
    title: str
    artist: str
    album: str | None = None
    year: int
    label: str | None = None

tracks: list[Track] = [
    {
        "id": 1,
        "title": "Here comes the sun",
        "artist": "Beatles",
        "album": "Abby Road",
        "year": 1969,
        "label": "Apple Records",
    },
    {
        "id": 2,
        "title": "Song 2",
        "artist": "Blur",
        "album": "Blur",
        "year": 1997,
        "label": "Parlophone",
    },
    {
        "id": 3,
        "title": "High & Dry",
        "artist": "Radiohead",
        "album": "The Bends",
        "year": 1995,
        "label": "EMI",
    },
    {
        "id": 4,
        "title": "The Rain Song",
        "artist": "Le Zeppelin",
        "album": "Houses of The Holy",
        "year": 1973,
        "label": "Atlantic Records",
    },
]

@router.get("")
async def get_tracks(skip: int = 0, limit: int = 1, q: str | None = None):
    matched: [Track] = []

    if q:
        for track in tracks:
            if track["artist"].lower() == q.lower():
                matched.append(track)

    else:
        return tracks[skip : skip + limit]
    return matched[skip : skip + limit]

@router.get("/{track_id}")
async def get_track_by_id(track_id: int):
    for track in tracks:
        if track["id"] == track_id:
            return track
    raise HTTPException(status_code=404, detail="Track not found")

@router.post("")
async def create_track(track: Track):
  tracks.append(track)
  return track
```

### part-4
Authentication & Authorization using HTTPBasic

```
app
 ├── __init__.py
 ├── main.py
 ├── routers
     ├── __init__.py
     ├── tracks.py
     ├── users.py
dockerfile
docker-compose.yml
requirements.txt
```

```python
./app/main.py

from fastapi import FastAPI
from routers import tracks, users
# from pydantic import BaseModel

app = FastAPI()

fallback = {404: {"description": "NotFound"}}
app.include_router(tracks.router, prefix="/tracks", tags=["Tracks"])
app.include_router(users.router, prefix="/users", tags=["Users"])
# app.include_router(tracks.router, prefix="/tracks", tags=["Tracks"], response=fallback)

# class Track(BaseModel):
#     title: str
#     artist: str
#     album: str | None = None
#     year: int
#     label: str | None = None

# tracks: list[Track] = []


@app.get("/")
async def root():
    return {"Hello": "World!"}

# @app.get("/tracks")
# async def get_tracks():
#     return tracks

# @app.post("/tracks")
# async def create_track(track: Track):
#   tracks.append(track)
#   return track
```

```python
# ./app/routers/tracks.py

from fastapi import APIRouter, HTTPException
from pydantic import BaseModel


router = APIRouter()
# router = APIRouter(
#   prefix = "/tracks",
#   tags=["Tracks"],
# #   response = ({404: {"description" : "Not Found"}})
# )

class Track(BaseModel):
    id: int
    title: str
    artist: str
    album: str | None = None
    year: int
    label: str | None = None

tracks: list[Track] = [
    {
        "id": 1,
        "title": "Here comes the sun",
        "artist": "Beatles",
        "album": "Abby Road",
        "year": 1969,
        "label": "Apple Records",
    },
    {
        "id": 2,
        "title": "Song 2",
        "artist": "Blur",
        "album": "Blur",
        "year": 1997,
        "label": "Parlophone",
    },
    {
        "id": 3,
        "title": "High & Dry",
        "artist": "Radiohead",
        "album": "The Bends",
        "year": 1995,
        "label": "EMI",
    },
    {
        "id": 4,
        "title": "The Rain Song",
        "artist": "Le Zeppelin",
        "album": "Houses of The Holy",
        "year": 1973,
        "label": "Atlantic Records",
    },
]

@router.get("")
async def get_tracks(skip: int = 0, limit: int = 1, q: str | None = None):
    matched: [Track] = []

    if q:
        for track in tracks:
            if track["artist"].lower() == q.lower():
                matched.append(track)

    else:
        return tracks[skip : skip + limit]
    return matched[skip : skip + limit]

@router.get("/{track_id}")
async def get_track_by_id(track_id: int):
    for track in tracks:
        if track["id"] == track_id:
            return track
    raise HTTPException(status_code=404, detail="Track not found")

@router.post("")
async def create_track(track: Track):
  tracks.append(track)
  return track
```

```python
import secrets
from typing import Annotated
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from operator import attrgetter

router = APIRouter()

security = HTTPBasic()

user = {"username": "john", "password": "password"}

@router.get("/me")
async def get_current_user(
  credentials: Annotated[HTTPBasicCredentials, Depends(security)]
):
  username, password = attrgetter("username", "password")(credentials)
  is_correct_user = secrets.compare_digest(username, user["username"])
  is_correct_password = secrets.compare_digest(password, user["password"])

  if not is_correct_user and not is_correct_password:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
        )
  return {"username": username}
```

### part-5
옛날 방식 세션 쿠키를 사용

```
app
 ├── __init__.py
 ├── main.py
 ├── dependencies.py
 ├── routers
     ├── __init__.py
     ├── tracks.py
     ├── users.py
     ├── login.py
     ├── logout.py
dockerfile
docker-compose.yml
requirements.txt
```

```
# ./requirements.txt

fastapi==0.103.2
pydantic==2.4.2
uvicorn==0.23.2
python-multipart==0.0.6
itsdangerous==2.1.2
```

```python
# ./app/main.py

from fastapi import FastAPI
from routers import tracks, users, login, logout
from starlette.middleware.sessions import SessionMiddleware
from dependencies import MY_SESSION_ID
# from pydantic import BaseModel

app = FastAPI()
app.add_middleware(
    SessionMiddleware,
    same_site="strict",
    session_cookie=MY_SESSION_ID,
    secret_key="mysecret",
)


fallback = {404: {"description": "NotFound"}}
app.include_router(tracks.router, prefix="/tracks", tags=["Tracks"])
app.include_router(users.router, prefix="/users", tags=["Users"])
app.include_router(login.router, prefix="/login", tags=["Login"])
app.include_router(logout.router, prefix="/logout", tags=["Logout"])
# app.include_router(tracks.router, prefix="/tracks", tags=["Tracks"], response=fallback)

# class Track(BaseModel):
#     title: str
#     artist: str
#     album: str | None = None
#     year: int
#     label: str | None = None

# tracks: list[Track] = []


@app.get("/")
async def root():
    return {"Hello": "World!"}

# @app.get("/tracks")
# async def get_tracks():
#     return tracks

# @app.post("/tracks")
# async def create_track(track: Track):
#   tracks.append(track)
#   return track
```

```python
# ./app/dependencies.py
MY_SESSION_ID = "my_session_id"
user = {"username": "john", "password": "password"}
sessions = {}
```

```python
# ./app/routers/tracks.py

from fastapi import APIRouter, HTTPException
from pydantic import BaseModel


router = APIRouter()
# router = APIRouter(
#   prefix = "/tracks",
#   tags=["Tracks"],
# #   response = ({404: {"description" : "Not Found"}})
# )

class Track(BaseModel):
    id: int
    title: str
    artist: str
    album: str | None = None
    year: int
    label: str | None = None

tracks: list[Track] = [
    {
        "id": 1,
        "title": "Here comes the sun",
        "artist": "Beatles",
        "album": "Abby Road",
        "year": 1969,
        "label": "Apple Records",
    },
    {
        "id": 2,
        "title": "Song 2",
        "artist": "Blur",
        "album": "Blur",
        "year": 1997,
        "label": "Parlophone",
    },
    {
        "id": 3,
        "title": "High & Dry",
        "artist": "Radiohead",
        "album": "The Bends",
        "year": 1995,
        "label": "EMI",
    },
    {
        "id": 4,
        "title": "The Rain Song",
        "artist": "Le Zeppelin",
        "album": "Houses of The Holy",
        "year": 1973,
        "label": "Atlantic Records",
    },
]

@router.get("")
async def get_tracks(skip: int = 0, limit: int = 1, q: str | None = None):
    matched: [Track] = []

    if q:
        for track in tracks:
            if track["artist"].lower() == q.lower():
                matched.append(track)

    else:
        return tracks[skip : skip + limit]
    return matched[skip : skip + limit]

@router.get("/{track_id}")
async def get_track_by_id(track_id: int):
    for track in tracks:
        if track["id"] == track_id:
            return track
    raise HTTPException(status_code=404, detail="Track not found")

@router.post("")
async def create_track(track: Track):
  tracks.append(track)
  return track
```

```python
# ./app/routers/users.py

# import secrets
# from typing import Annotated
# from fastapi import APIRouter, Depends, HTTPException, status
# from fastapi.security import HTTPBasic, HTTPBasicCredentials
# from operator import attrgetter
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import APIKeyCookie
from dependencies import MY_SESSION_ID
from fastapi.requests import Request


router = APIRouter()

security = APIKeyCookie(name=MY_SESSION_ID)
# security = HTTPBasic()

# user = {"username": "john", "password": "password"}

@router.get("/me", dependencies=[Depends(security)])
async def get_current_user(
    request: Request,
):
    if not request.session:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
        )
    return request.session
# @router.get("/me")
# async def get_current_user(
#   credentials: Annotated[HTTPBasicCredentials, Depends(security)]
# ):
#   username, password = attrgetter("username", "password")(credentials)
#   is_correct_user = secrets.compare_digest(username, user["username"])
#   is_correct_password = secrets.compare_digest(password, user["password"])

#   if not is_correct_user and not is_correct_password:
#         raise HTTPException(
#             status_code=status.HTTP_401_UNAUTHORIZED,
#             detail="Incorrect username or password",
#         )
#   return {"username": username}
```

```python
# ./app/routers/login.py

import secrets
from typing import Annotated
from fastapi import APIRouter, HTTPException, status, Form
# from fastapi.security import APIKeyCookie
from fastapi.requests import Request
from dependencies import user

router = APIRouter()

# security = APIKeyCookie(name=MY_SESSION_ID)


@router.post("")
async def login(
    request: Request,
    username: Annotated[str, Form()],
    password: Annotated[str, Form()],
):
    is_correct_user = secrets.compare_digest(username, user["username"])
    is_correct_password = secrets.compare_digest(password, user["password"])

    if not is_correct_user or not is_correct_password:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
        )
    request.session.update({"username": username})
    return {"success": True}
```

```python
# ./app/routers/logout.py

from fastapi import APIRouter
from fastapi.security import APIKeyCookie
from fastapi.requests import Request
from dependencies import MY_SESSION_ID

router = APIRouter()

security = APIKeyCookie(name=MY_SESSION_ID)


@router.post("")
async def logout(
    request: Request,
):
    request.session.clear()
    return {"success": True}
```
